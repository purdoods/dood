package co.doodrideshare.dood.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import co.doodrideshare.dood.R;
import co.doodrideshare.dood.databaseHelper.FirebaseDatabaseHelper;
import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.models.User;
import co.doodrideshare.dood.utilities.Utility;

/**
 * Created by Nishant on 11/9/2017.
 */

public class TripDialogFragment extends DialogFragment {

    private Trip mTrip;
    private String mRiderUsername;

    public static TripDialogFragment newInstance(String tripHashcode) {
        TripDialogFragment f = new TripDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("trip", tripHashcode);
        f.setArguments(args);

        return f;
    }

    public static TripDialogFragment newInstance(String tripHashcode, String riderUsername) {
        TripDialogFragment f = new TripDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("trip", tripHashcode);
        args.putString("riderUsername", riderUsername);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTrip = new Trip(getArguments().getString("trip"));
        mRiderUsername = getArguments().getString("riderUsername");

        // Pick a style based on the num.
        /*
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        switch ((mNum-1)%6) {
            case 1: style = DialogFragment.STYLE_NO_TITLE; break;
            case 2: style = DialogFragment.STYLE_NO_FRAME; break;
            case 3: style = DialogFragment.STYLE_NO_INPUT; break;
            case 4: style = DialogFragment.STYLE_NORMAL; break;
            case 5: style = DialogFragment.STYLE_NORMAL; break;
            case 6: style = DialogFragment.STYLE_NO_TITLE; break;
            case 7: style = DialogFragment.STYLE_NO_FRAME; break;
            case 8: style = DialogFragment.STYLE_NORMAL; break;
        }
        switch ((mNum-1)%6) {
            case 4: theme = android.R.style.Theme_Holo; break;
            case 5: theme = android.R.style.Theme_Holo_Light_Dialog; break;
            case 6: theme = android.R.style.Theme_Holo_Light; break;
            case 7: theme = android.R.style.Theme_Holo_Light_Panel; break;
            case 8: theme = android.R.style.Theme_Holo_Light; break;
        }
        */
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog, container, false);

        TextView textViewDestination = v.findViewById(R.id.textViewDestination);
        TextView time = v.findViewById(R.id.time);
        final TextView name = v.findViewById(R.id.name);
        final TextView textViewMajor = v.findViewById(R.id.textViewMajor);
        final ImageButton phoneCall = v.findViewById(R.id.phoneCall);
        final ImageButton message = v.findViewById(R.id.message);
        final Button buttonAcceptCancel = v.findViewById(R.id.buttonAcceptCancel);


        if (mTrip != null) {
            textViewDestination.setText(mTrip.getTo());
            time.setText(Utility.getFormattedTime(Double.parseDouble(mTrip.getTime())));
            String myUsername = Utility.getUser(getContext()).getUsername();
            String otherPersonUsername = myUsername;

            // If you are the rider, you can cancel trip
            if (!mTrip.getDriverUsername().equals(myUsername)) {
                otherPersonUsername = mTrip.getDriverUsername();

                buttonAcceptCancel.setText("CANCEL");

                buttonAcceptCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // CANCEL TRIP
                        // Remove rider's username from trip on the database
                    }
                });
            }
            else if (mRiderUsername != null) {
                // Else you are the driver, you can accept or cancel trip
                otherPersonUsername = mRiderUsername;

                if (mTrip.getRiderUsername() != null && mTrip.getRiderUsername().equals(mRiderUsername)) {
                    // Already accepted trip before, cancel it then
                    buttonAcceptCancel.setText("CANCEL");
                }
                else {
                    buttonAcceptCancel.setText("ACCEPT");
                }

                buttonAcceptCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (buttonAcceptCancel.getText().toString().equals("ACCEPT")) {
                            acceptTrip();
                        }
                        else {
                            cancelTrip();
                        }
                    }
                });
            }

            // Fetching and displaying other user's information
            FirebaseDatabaseHelper.getUserInfo(otherPersonUsername, new OnUserFetchCompleteListener() {
                @Override
                public void onUserFetchComplete(User user) {
                    name.setText(user.getName());
                    textViewMajor.setText(user.getMajor());
                    String phoneNumber = user.getPhoneNumber();
                    phoneCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Start phone call

                        }
                    });
                    message.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Send SMS
                        }
                    });
                }
            });
        }

        return v;
    }

    private void cancelTrip() {
    }

    private void acceptTrip() {

    }

    public interface OnUserFetchCompleteListener {
        void onUserFetchComplete(User user);
    }
}
