package co.doodrideshare.dood.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

import co.doodrideshare.dood.activities.DestinationActivity;

/**
 * Created by Nishant on 11/5/2017.
 */

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, false);
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        DestinationActivity destinationActivity = (DestinationActivity) getActivity();
        if (minute / 30 == 0) {
            minute = 0;
        } else if (minute % 30 < 15){
            minute = 30;
        } else {
            minute = 0;
            hourOfDay++;
        }
        destinationActivity.setTime(hourOfDay, minute);
    }
}
