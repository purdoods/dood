package co.doodrideshare.dood.adapters;

/**
 * Created by Nishant on 11/5/2017.
 */

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import co.doodrideshare.dood.R;
import co.doodrideshare.dood.activities.CommunityActivity;
import co.doodrideshare.dood.databaseHelper.FirebaseDatabaseHelper;
import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.models.User;
import co.doodrideshare.dood.utilities.Utility;

/**
 * Created by Nishant on 11/5/2017.
 */

public class TripRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_TRIP = 1;
    private Context mContext;
    private ArrayList<Trip> trips;
    private ArrayList<User> users;
    private String mSelectedPhoneNumber = "";
    private Trip mSelectedTrip = null;
    private boolean isOngoingTrips = false;

    public TripRecyclerAdapter(Context context, ArrayList<Trip> trips, boolean isOngoingTrips) {
        this.mContext = context;
        this.trips = trips;
        this.isOngoingTrips = isOngoingTrips;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }


    class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private TextView textViewDestination;
        private TextView textViewMajor;
        private TextView textViewPrice;
        private RelativeLayout primaryFunction;
        private Button buttonDelete;
        private Button buttonAccepted;
        private View typeIndicator;
        private TextView textViewType;

        UserViewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.name);
            textViewDestination = itemView.findViewById(R.id.textViewDestination);
            textViewMajor = itemView.findViewById(R.id.textViewMajor);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            primaryFunction = itemView.findViewById(R.id.primaryFunction);
            buttonDelete = itemView.findViewById(R.id.buttonDelete);
            buttonAccepted = itemView.findViewById(R.id.buttonAccepted);
            typeIndicator = itemView.findViewById(R.id.typeIndicator);
            textViewType = itemView.findViewById(R.id.type);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewHeader;

        HeaderViewHolder(View itemView) {
            super(itemView);
            textViewHeader = itemView.findViewById(R.id.header);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isOngoingTrips) {
            if (position == 0) {
                return TYPE_HEADER;
            }
            else {
                return TYPE_TRIP;
            }
        }
        if (getItemCount() > 4) {
            if (position == 0 || position == 5) {
                return TYPE_HEADER;
            }
            else {
                return TYPE_TRIP;
            }
        }
        else {
            if (position == 0) {
                return TYPE_HEADER;
            }
            else {
                return TYPE_TRIP;
            }
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        if (viewType == TYPE_HEADER) {
            // create a new view
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_header, parent, false);
            // set the view's size, margins, padding and layout parameters
            return new HeaderViewHolder(itemView);
        }

        // create a new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_trip, parent, false);
        // set the view's size, margins, padding and layout parameters
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        if (getItemViewType(position) == TYPE_HEADER) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            if (position == 0) {
                if (isOngoingTrips) {
                    headerViewHolder.textViewHeader.setText("ONGOING TRIPS");
                }
                else {
                    headerViewHolder.textViewHeader.setText("TOP MATCHES");
                }
            }
            else {
                headerViewHolder.textViewHeader.setText("MORE RECOMMENDATIONS");
            }
        }
        else {
            UserViewHolder userViewHolder = (UserViewHolder) viewHolder;
            final Trip trip;

            // to account for headers
            if (position < 5) {
                trip = trips.get(position - 1);
            }
            else if (!isOngoingTrips) {
                trip = trips.get(position - 2);
            }
            else {
                trip = trips.get(position - 1);
            }

            User currentUser = null;
            if (users != null) {
                if (trip.getType().equals(Trip.TYPE_NEED)) {
                    for (User user : users) {
                        if (user.username.equals(trip.getRiderUsername())) {
                            currentUser = user;
                            break;
                        }
                    }
                }
                else if (trip.getType().equals(Trip.TYPE_OFFER)) {
                    for (User user : users) {
                        if (user.username.equals(trip.getDriverUsername())) {
                            currentUser = user;
                            break;
                        }
                    }
                }

            }

            if (currentUser != null) {
                userViewHolder.textViewName.setText(currentUser.getName());
                userViewHolder.textViewMajor.setText(currentUser.getMajor());
                if (currentUser.getUsername().equals(Utility.getUser(mContext).getUsername())) {
                    userViewHolder.buttonAccepted.setVisibility(View.VISIBLE);
                    userViewHolder.buttonDelete.setVisibility(View.VISIBLE);
                }
                else {
                    userViewHolder.buttonAccepted.setVisibility(View.GONE);
                    userViewHolder.buttonDelete.setVisibility(View.GONE);
                }
            }
            else {
                userViewHolder.textViewName.setText("Loading...");
                userViewHolder.textViewMajor.setText("Loading...");
            }

            if (trip.getType().equals(Trip.TYPE_NEED)) {
                userViewHolder.typeIndicator.setBackgroundColor(mContext.getResources().getColor(R.color.green));
                userViewHolder.textViewType.setText("(N)");
                userViewHolder.textViewType.setTextColor(mContext.getResources().getColor(R.color.green));
                userViewHolder.buttonAccepted.setText("DRIVER FOUND");
            }
            else if (trip.getType().equals(Trip.TYPE_OFFER)) {
                userViewHolder.typeIndicator.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                userViewHolder.textViewType.setText("(O)");
                userViewHolder.textViewType.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                userViewHolder.buttonAccepted.setText("RIDER FOUND");
            }

            userViewHolder.textViewPrice.setText(Utility.getEstimatedPrice(trip.getFrom(), trip.getTo()));
            userViewHolder.textViewDestination.setText("from " + trip.getFrom() + "\nto " + trip.getTo() + "\n" +
                    Utility.getFormattedTime(Double.parseDouble(trip.getTime())));

            final CommunityActivity communityActivity = (CommunityActivity) mContext;
            userViewHolder.buttonAccepted.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                            .setMessage("Let us know that you found a rider or a driver for your trip. We will remove this trip from the list." +
                                    " Please add another trip if you have more space in your car.")
                            .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    FirebaseDatabaseHelper.addAcceptedTrips(trip);
                                    communityActivity.fetchTrips(communityActivity.mRequestedTrip);
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    builder.show();
                }
            });

            userViewHolder.buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                            .setMessage("Are you sure you want to delete the trip?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    FirebaseDatabaseHelper.deleteTrip(trip);
                                    communityActivity.fetchTrips(communityActivity.mRequestedTrip);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    builder.show();
                }
            });

            final User finalCurrentUser = currentUser;

            try {
                if (!currentUser.getEmail().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail())) {
                    userViewHolder.primaryFunction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mSelectedPhoneNumber = finalCurrentUser.getPhoneNumber();
                            mSelectedTrip = trip;
                            CommunityActivity communityActivity = (CommunityActivity) mContext;
                            communityActivity.checkPermission();
                            if (ContextCompat.checkSelfPermission(mContext,
                                    Manifest.permission.SEND_SMS)
                                    == PackageManager.PERMISSION_GRANTED) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setMessage("Do you want to send the driver an SMS to request a ride? \n" +
                                        "P.S. Please negotiate price with the driver. We'll add a payment system in the app soon :)")
                                        .setPositiveButton("SEND SMS", dialogClickListener)
                                        .setNegativeButton("CANCEL", dialogClickListener).show();
                            }
                            else {
                                ActivityCompat.requestPermissions(communityActivity,
                                        new String[]{Manifest.permission.SEND_SMS},
                                        CommunityActivity.MY_PERMISSIONS_REQUEST_SEND_SMS);
                            }
                /*
                try {
                    FCMHelper fcmHelper = new FCMHelper();
                    fcmHelper.sendPush(finalCurrentUser.getRegistrationToken(), trip, mContext);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }*/
                        }
                    });
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int choice) {
            switch (choice) {
                case DialogInterface.BUTTON_POSITIVE:
                    if (!mSelectedPhoneNumber.isEmpty() && mSelectedTrip != null) {
                        try {
                            sendSMS(mSelectedPhoneNumber, mSelectedTrip);
                            FirebaseDatabaseHelper.addPotentialTripToDatabase(mSelectedTrip, mContext);
                            Toast.makeText(mContext, "SMS sent!", Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(mContext, "Error sending SMS!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    Toast.makeText(mContext, "Request Cancelled", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void sendSMS(String phoneNumber, Trip mSelectedTrip) {
        CommunityActivity communityActivity = (CommunityActivity) mContext;
        if (ContextCompat.checkSelfPermission(communityActivity,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(communityActivity,
                    new String[]{Manifest.permission.SEND_SMS},
                    CommunityActivity.MY_PERMISSIONS_REQUEST_SEND_SMS);

        }
        else {
            Log.d("adapter", "sending SMS to " + phoneNumber);
            User user = Utility.getUser(mContext);
            String time = Utility.getFormattedTime(Double.parseDouble(mSelectedTrip.getTime()));
            String message = "";
            if (mSelectedTrip.getType().equals(Trip.TYPE_OFFER)) {
                message = "Hi Dood! My name is " + user.getName() +
                        ". I would like to get a ride with you from " + mSelectedTrip.getFrom() + " to " +
                        mSelectedTrip.getTo() + " on " + time + ".";
            }
            else if (mSelectedTrip.getType().equals(Trip.TYPE_NEED)) {
                message = "Hi Dood! My name is " + user.getName() +
                        ". I can give you a ride from " + mSelectedTrip.getFrom() + " to " +
                        mSelectedTrip.getTo() + " on " + time + ". Would you like to ride with me for about "
                        + Utility.getEstimatedPrice(mSelectedTrip.getFrom(), mSelectedTrip.getTo()) + "?";
            }

            Log.d("TRIP", message);
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber, null, message, null, null);
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (isOngoingTrips) {
            return trips.size() + 1;
        }
        else if (trips.size() > 4) {
            return trips.size() + 2;
        }
        else {
            return trips.size() + 1;
        }
    }
}
