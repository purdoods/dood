package co.doodrideshare.dood.services;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import co.doodrideshare.dood.databaseHelper.FirebaseDatabaseHelper;
import co.doodrideshare.dood.utilities.Utility;

/**
 * Created by Nishant on 2/28/2016.
 */
public class MyInstanceIDListenerService extends FirebaseInstanceIdService {
    DatabaseReference mDatabaseReference;

    private static final String TAG = "MyInstanceIDLS";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is also called
     * when the InstanceID token is initially generated, so this is where
     * you retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
        Utility.setRegistrationToken(getApplicationContext(), refreshedToken);
    }

    public void sendRegistrationToServer(String refreshedToken) {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        FirebaseDatabaseHelper.updateRegTokenOnDatabase(mDatabaseReference, refreshedToken);
    }
    // [END refresh_token]
}
