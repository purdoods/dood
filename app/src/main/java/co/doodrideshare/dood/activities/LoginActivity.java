package co.doodrideshare.dood.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import co.doodrideshare.dood.R;
import co.doodrideshare.dood.databaseHelper.FirebaseDatabaseHelper;
import co.doodrideshare.dood.models.User;
import co.doodrideshare.dood.utilities.Utility;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LOGIN_ACTIVITY";
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Button signUpButton = findViewById(R.id.buttonSignUp);
        final EditText email = findViewById(R.id.editTextEmail);
        final EditText major = findViewById(R.id.editTextMajor);
        final EditText number = findViewById(R.id.editTextNumber);
        final EditText password = findViewById(R.id.editTextPassword);
        final EditText name = findViewById(R.id.editTextName);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            // Reload user in case of email verification
            Task<Void> task = currentUser.reload();
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                }
            });

            try {
                if (Utility.getUser(this).getName() != null && Utility.getUser(this).getEmail().equals(currentUser.getEmail())) {
                    updateRegistrationToken();
                    if (!currentUser.isEmailVerified()) {
                        Snackbar.make(signUpButton, "Please verify your email", Snackbar.LENGTH_SHORT).show();
                    }
                    else {
                        successfulLogin();
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!checkPass(password.getText().toString())) {
                    Snackbar.make(signUpButton,
                            "Please enter a password > 5 characters",
                            Snackbar.LENGTH_LONG).show();
                }
                else if (!checkNumber(number.getText().toString())) {
                    Snackbar.make(signUpButton,
                            "Please enter your 10 digit phone number. Please enter your correct " +
                                    "phone number to contact other students for rides.",
                            Snackbar.LENGTH_LONG).show();
                }
                else if (!checkMajor(major.getText().toString())) {
                    Snackbar.make(signUpButton,
                            "Please enter your major. eg. Computer Science ", Snackbar.LENGTH_LONG).show();
                }
                else if (!checkEmail(email.getText().toString())) {
                    Snackbar.make(signUpButton,
                            "Please enter your working Purdue email address. Your email will be verified.", Snackbar.LENGTH_LONG).show();
                }
                else if (!checkName(name.getText().toString())) {
                    Snackbar.make(signUpButton,
                            "Please enter your full name. eg. Elton John", Snackbar.LENGTH_LONG).show();
                }
                else {
                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // NEW SIGN UP
                                        Log.d(TAG, "createUserWithEmail:success");
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        sendEmailVerificationEmail(user);
                                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                                        User currentUser = new User(name.getText().toString(), user.getEmail(),
                                                major.getText().toString(), number.getText().toString());
                                        FirebaseDatabaseHelper.updateUserInformation(databaseReference,
                                                currentUser);
                                        Utility.setUser(LoginActivity.this, currentUser);
                                        updateRegistrationToken();

                                        Snackbar.make(signUpButton,
                                                "Please check your email for verification.",
                                                Snackbar.LENGTH_LONG).show();
                                    }
                                    else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());

                                        // Check if account already exists and try signing in
                                        mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                                                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                                        if (task.isSuccessful()) {
                                                            // Sign in success, update UI with the signed-in user's information
                                                            Log.d(TAG, "signInWithEmail:success");
                                                            FirebaseUser user = mAuth.getCurrentUser();
                                                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                                                            User currentUser = new User(name.getText().toString(), user.getEmail(),
                                                                    major.getText().toString(), number.getText().toString());
                                                            FirebaseDatabaseHelper.updateUserInformation(databaseReference,
                                                                    currentUser);
                                                            Utility.setUser(LoginActivity.this, currentUser);
                                                            updateRegistrationToken();

                                                            //If verified, proceed
                                                            if (user.isEmailVerified()) {
                                                                Snackbar.make(signUpButton, "Signed In!",
                                                                        Snackbar.LENGTH_SHORT).addCallback(new Snackbar.Callback() {
                                                                    @Override
                                                                    public void onDismissed(Snackbar snackbar, int event) {
                                                                        successfulLogin();
                                                                    }

                                                                    @Override
                                                                    public void onShown(Snackbar snackbar) {
                                                                    }
                                                                }).show();
                                                            }
                                                            else {
                                                                Snackbar.make(signUpButton,
                                                                        "Please check your email for verification.",
                                                                        Snackbar.LENGTH_LONG).show();
                                                            }
                                                        }
                                                        else {
                                                            // If sign in fails, display a message to the user.
                                                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                                                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                                                    Toast.LENGTH_SHORT).show();
                                                        }

                                                    }
                                                });
                                    }
                                }
                            });

                }
            }
        });
    }

    private void updateRegistrationToken() {
        String regToken = Utility.getRegistrationToken(this);
        if (regToken != null) {
            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
            FirebaseDatabaseHelper.updateRegTokenOnDatabase(databaseReference, Utility.getRegistrationToken(this));
        }
    }

    private boolean checkName(String name) {
        return !name.isEmpty() && name.contains(" ");
    }

    private void successfulLogin() {
        startActivity(new Intent(LoginActivity.this, DestinationActivity.class));
        finish();
    }

    private boolean checkEmail(String email) {
        return email.contains("@purdue.edu");
    }

    private boolean checkMajor(String major) {
        return true;
    }

    private boolean checkNumber(String number) {
        return number.length() == 10;
    }

    private boolean checkPass(String password) {
        return password.length() > 4;
    }

    private void sendEmailVerificationEmail(FirebaseUser user) {
        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Email sent.");
                        }
                    }
                });
    }
}
