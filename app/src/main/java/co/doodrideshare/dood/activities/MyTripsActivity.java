package co.doodrideshare.dood.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import co.doodrideshare.dood.R;
import co.doodrideshare.dood.adapters.TripRecyclerAdapter;
import co.doodrideshare.dood.databaseHelper.FirebaseDatabaseHelper;
import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.models.User;
import co.doodrideshare.dood.utilities.Utility;

public class MyTripsActivity extends AppCompatActivity {

    RecyclerView mUserRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<Trip> mTrips;
    private TripRecyclerAdapter mAdapter;
    private ArrayList<User> mUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trips);

        mUserRecyclerView = findViewById(R.id.user_recycler_view);

        mUserRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mUserRecyclerView.setLayoutManager(mLayoutManager);

        fetchTrips(Utility.getUser(this));
    }


    private void fetchTrips(User user) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        mTrips = new ArrayList<>();

        FirebaseDatabaseHelper.getTrips(databaseReference, user, this, new DestinationActivity.OnFetchCompleteListener() {
            @Override
            public void fetchComplete(ArrayList<Trip> trips) {
                mTrips = trips;
                Log.d("COMMUNITY_ACTIVITY", "Size of trips: " + trips.size());
                // Sort and select
                mAdapter = new TripRecyclerAdapter(MyTripsActivity.this, mTrips, true);
                mAdapter.setUsers(mUsers);
                mUserRecyclerView.setAdapter(mAdapter);
            }
        });

        FirebaseDatabaseHelper.getUsers(databaseReference, new DestinationActivity.OnUserFetchCompleteListener() {
            @Override
            public void fetchComplete(ArrayList<User> users) {
                mUsers = users;
                Log.d("COMMUNITY_ACTIVITY", "Size of users: " + users.size());
                if (mAdapter != null) {
                    mAdapter = new TripRecyclerAdapter(MyTripsActivity.this, mTrips, true);
                    mAdapter.setUsers(mUsers);
                    mUserRecyclerView.setAdapter(mAdapter);
                }
            }
        });
    }

}
