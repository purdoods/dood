package co.doodrideshare.dood.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import co.doodrideshare.dood.R;
import co.doodrideshare.dood.adapters.TripRecyclerAdapter;
import co.doodrideshare.dood.databaseHelper.FirebaseDatabaseHelper;
import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.models.User;
import co.doodrideshare.dood.utilities.Utility;

public class CommunityActivity extends AppCompatActivity {

    RecyclerView mUserRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<Trip> mTrips;
    private TripRecyclerAdapter mAdapter;
    private ArrayList<User> mUsers;
    public static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
    private BroadcastReceiver mReceiver;
    private TextView mTextViewNoTripsMessage;
    public Trip mRequestedTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Doods of Purdue");

        String mode = "";
        if (getIntent().getStringExtra("trip") == null) {
            mRequestedTrip = null;
        }
        else {
            mRequestedTrip = new Trip(getIntent().getStringExtra("trip"));
            mode = getIntent().getStringExtra("mode");
        }


        mUserRecyclerView = findViewById(R.id.user_recycler_view);
        mTextViewNoTripsMessage = findViewById(R.id.textViewNoTripsMessage);
        Button feedbackButton = findViewById(R.id.feedback);

        feedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFeedbackAlertDialog();
            }
        });

        mTextViewNoTripsMessage.setVisibility(View.GONE);
        mUserRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mUserRecyclerView.setLayoutManager(mLayoutManager);

        fetchTrips(mRequestedTrip);

        checkPermission();
    }

    private void showFeedbackAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CommunityActivity.this);
        alertDialog.setMessage("Send a personal message/feedback to developers");
        alertDialog.setTitle("Chat");

        final EditText input = new EditText(CommunityActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(24, 8, 24, 8);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String feedback = input.getText().toString();
                FirebaseDatabaseHelper.sendFeedback(Utility.getUser(CommunityActivity.this).getUsername(), feedback);
                Snackbar.make(mUserRecyclerView, "Thank you for your feedback! We'll work on it :)", Snackbar.LENGTH_LONG).show();
            }
        });

        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        alertDialog.show();
    }

    public void fetchTrips(final Trip requestedTrip) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        mTrips = new ArrayList<>();
        mUsers = new ArrayList<>();

        FirebaseDatabaseHelper.getTrips(databaseReference, requestedTrip, this, new DestinationActivity.OnFetchCompleteListener() {
            @Override
            public void fetchComplete(ArrayList<Trip> trips) {
                mTrips = trips;
                Log.d("COMMUNITY_ACTIVITY", "Size of trips: " + trips.size());
                // Sort and select
                if (requestedTrip == null) {
                    mAdapter = new TripRecyclerAdapter(CommunityActivity.this, mTrips, true);
                } else {
                    mAdapter = new TripRecyclerAdapter(CommunityActivity.this, mTrips, false);
                }
                mAdapter.setUsers(mUsers);
                mUserRecyclerView.setAdapter(mAdapter);
                if (mTrips.isEmpty()) {
                    mTextViewNoTripsMessage.setVisibility(View.VISIBLE);
                }
                else {
                    mTextViewNoTripsMessage.setVisibility(View.GONE);
                }
            }
        });

        FirebaseDatabaseHelper.getUsers(databaseReference, new DestinationActivity.OnUserFetchCompleteListener() {
            @Override
            public void fetchComplete(ArrayList<User> users) {
                mUsers = users;
                Log.d("COMMUNITY_ACTIVITY", "Size of users: " + users.size());
                if (mAdapter != null) {
                    if (requestedTrip == null) {
                        mAdapter = new TripRecyclerAdapter(CommunityActivity.this, mTrips, true);
                    } else {
                        mAdapter = new TripRecyclerAdapter(CommunityActivity.this, mTrips, false);
                    }
                    mAdapter.setUsers(mUsers);
                    mUserRecyclerView.setAdapter(mAdapter);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                }
                else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        }
    }
}
