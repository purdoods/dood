package co.doodrideshare.dood.activities;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import co.doodrideshare.dood.R;
import co.doodrideshare.dood.databaseHelper.FirebaseDatabaseHelper;
import co.doodrideshare.dood.fragments.DatePickerFragment;
import co.doodrideshare.dood.fragments.TimePickerFragment;
import co.doodrideshare.dood.models.Price;
import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.models.User;
import co.doodrideshare.dood.utilities.Utility;

public class DestinationActivity extends AppCompatActivity {

    public static final String RIDER = "RIDER";
    public static final String DRIVER = "DRIVER";

    private Button mButtonAvenue;
    private Button mButtonEE;
    private Button mButtonPMU;
    private TextView mTextViewTime;
    private TextView mTextViewMode;
    private String mDestination = "";
    private String mFrom = "";
    private String mTo = "";
    private Button mButtonDone;
    private Long mTime;
    String mMode = RIDER;
    private Calendar calendar = Calendar.getInstance();
    private Spinner mSpinnerFrom;
    private Spinner mSpinnerTo;
    private Button mButtonMessageDev;
    private ImageView mImageViewFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);

        mTextViewTime = (TextView) findViewById(R.id.textViewTime);
        mTextViewMode = (TextView) findViewById(R.id.textViewMode);
        mSpinnerFrom = (Spinner) findViewById(R.id.spinnerFrom);
        mSpinnerTo = (Spinner) findViewById(R.id.spinnerTo);
        mButtonMessageDev = findViewById(R.id.buttonMessageDev);
        mButtonDone = (Button) findViewById(R.id.buttonDone);
        mImageViewFacebook = findViewById(R.id.facebook);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.locations, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerFrom.setAdapter(adapter);
        mSpinnerTo.setAdapter(adapter);
        final String[] array = DestinationActivity.this.getResources().getStringArray(R.array.locations);

        mSpinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                mFrom = mSpinnerFrom.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        mSpinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mTo = mSpinnerTo.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mButtonMessageDev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFeedbackAlertDialog();
            }
        });
        mImageViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Dood-128891491126822/")));
            }
        });


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mButtonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFrom.isEmpty() || mTo.isEmpty()) {
                    Snackbar.make(mButtonDone, "Please select locations", Snackbar.LENGTH_SHORT).show();
                }
                else if (mTo.equals(mFrom)) {
                    Snackbar.make(mButtonDone, "Please select different from and to locations", Snackbar.LENGTH_SHORT).show();
                }
                else if (mTime != null && Calendar.getInstance().getTimeInMillis() - mTime > 60000 /* more than 1 min diff */) {
                    Snackbar.make(mButtonDone, "Please select a time in the future", Snackbar.LENGTH_LONG).show();
                }
                else {
                    Intent intent = new Intent(DestinationActivity.this, CommunityActivity.class);
                    if (mTime == null) {
                        mTime = Calendar.getInstance().getTimeInMillis();
                    }

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    String username = user.getEmail().substring(0, user.getEmail().indexOf("@"));

                    Trip trip = new Trip();
                    if (mMode.equals(DRIVER)) {
                        trip = new Trip(UUID.randomUUID().toString(), mFrom, mTo, username, mTime.toString(), Trip.TYPE_OFFER);
                    }
                    else if (mMode.equals(RIDER)) {
                        trip = new Trip(UUID.randomUUID().toString(), mFrom, mTo, username, mTime.toString(), Trip.TYPE_NEED);
                    }
                    intent.putExtra("trip", trip.hashcode());
                    intent.putExtra("mode", mMode);

                    FirebaseDatabaseHelper.addTrip(trip);
                    startActivity(intent);
                }
            }
        });

        mTextViewTime.setPaintFlags(mTextViewTime.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTextViewTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        FirebaseDatabaseHelper.getPrices(new OnPricesReceived() {
            @Override
            public void fetchComplete(DataSnapshot snapshot) {
                Utility.price = snapshot.getValue(Price.class);
            }
        });
    }

    private void showFeedbackAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DestinationActivity.this);
        alertDialog.setMessage("Send a personal message/feedback to developers");
        alertDialog.setTitle("Chat");

        final EditText input = new EditText(DestinationActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(24, 8, 24, 8);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String feedback = input.getText().toString();
                FirebaseDatabaseHelper.sendFeedback(Utility.getUser(DestinationActivity.this).getUsername(), feedback);
                Snackbar.make(mButtonDone, "Thank you for your feedback! We'll work on it :)", Snackbar.LENGTH_LONG).show();
            }
        });

        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        alertDialog.show();
    }

    public void onDestinationSelected(View view) {
        boolean checked = ((RadioButton) view).isChecked();

//        switch (view.getId()) {
//            case R.id.radioButtonAvenue:
//                if (checked)
//                    mDestination = "Avenue";
//                break;
//            case R.id.radioButtonEE:
//                if (checked)
//                    mDestination = "EE";
//                break;
//            case R.id.radioButtonPMU:
//                if (checked)
//                    mDestination = "PMU";
//                break;
//        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_driver:
                    mTextViewMode.setText("I am driving");
                    mButtonDone.setText("POST A TRIP");
                    mMode = DRIVER;
                    return true;
                case R.id.navigation_rider:
                    mTextViewMode.setText("I need a ride");
                    mButtonDone.setText("GET A RIDE");
                    mMode = RIDER;
                    return true;
                case R.id.navigation_trips:
                    // Find most recent trip

                    // Latest trip for a rider is the latest accepted trip
                    startActivity(new Intent(DestinationActivity.this, CommunityActivity.class));

                    return true;
            }
            return false;
        }
    };

    public void setTime(int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        mTime = calendar.getTimeInMillis();
        mTextViewTime.setText("at " + Utility.getFormattedTime(calendar));
    }

    public void setDate(int year, int month, int day) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    public interface OnFetchCompleteListener {
        void fetchComplete(ArrayList<Trip> trips);
    }

    public interface OnUserFetchCompleteListener {
        void fetchComplete(ArrayList<User> users);
    }

    public interface OnPricesReceived {
        void fetchComplete(DataSnapshot snapshot);
    }
}
