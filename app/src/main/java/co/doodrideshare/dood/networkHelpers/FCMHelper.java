package co.doodrideshare.dood.networkHelpers;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.utilities.Utility;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Nishant on 1/16/2017.
 */

public class FCMHelper {

    /* Create a network request to Firebase to send notification
    * */

    public void sendPush(String toRegistrationToken, Trip trip, Context context) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(logging).build();
        httpClient.retryOnConnectionFailure();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://fcm.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        FCMService fcmService = retrofit.create(FCMService.class);
        try {
            String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
            String username = email.substring(0, email.indexOf("@"));
            String name = Utility.getUser(context).getName();
            Call<Response> response = fcmService.sendPush(new FCMRequestBody(
                    Utility.getRegistrationToken(context), toRegistrationToken, trip, username, name));

            response.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    Log.d("FCM", "HTTP RESPONSE SUCCESS: " + response.message());
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    Log.d("FCM", "HTTP RESPONSE FAILURE");
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "User not authorized!", Toast.LENGTH_SHORT).show();
        }


    }

    /*
    public void sendSilentPush(String registrationToken, Context context) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(logging).build();
        httpClient.retryOnConnectionFailure();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://fcm.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        FCMService fcmService = retrofit.create(FCMService.class);
        try {
            Call<Response> response = fcmService.sendPush(new FCMRequestBody());
            response.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    Log.d("FCM", "HTTP RESPONSE SUCCESS: " + response.body().getSuccess());
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    Log.d("FCM", "HTTP RESPONSE FAILURE");
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "User not authorized!", Toast.LENGTH_SHORT).show();
        }
    }
    */
}

