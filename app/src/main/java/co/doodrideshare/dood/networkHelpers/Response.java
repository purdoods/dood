package co.doodrideshare.dood.networkHelpers;

/**
 * Created by Nishant on 1/18/2017.
 */

public class Response {

    private int success;
    private int failure;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getFailure() {
        return failure;
    }

    public void setFailure(int failure) {
        this.failure = failure;
    }
}
