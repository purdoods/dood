package co.doodrideshare.dood.networkHelpers;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

interface FCMService {

    @Headers({
            "Authorization: key=AAAAajibpL4:APA91bEywKS1xvYPDYARdZBYr-iYkjEqU-IiIaivMD_HkETJU5QI_85wSIperYfmsKNKOjctvg-8U6G4JZYp5-cqsiZMGiIGQmJeOUg1AYFKqpcK-R4nBmJesJRQR1zsixEm9FMQPCag",
            "Content-Type: application/json"
    })
    @POST("fcm/send")
    Call<Response> sendPush(@Body FCMRequestBody body);
}
