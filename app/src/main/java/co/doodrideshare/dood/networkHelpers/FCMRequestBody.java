package co.doodrideshare.dood.networkHelpers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.utilities.Utility;

/**
 * Created by Nishant on 1/18/2017.
 */

class FCMRequestBody {
    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("data")
    @Expose
    private Data data;

    FCMRequestBody(String myRegistrationToken, String toRegistrationToken, Trip trip, String username, String name) {
        this.to = toRegistrationToken;
        this.data = new Data(myRegistrationToken, trip.hashcode(), username, name);
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    private class Data {

        @SerializedName("registrationToken")
        @Expose
        private String registrationToken;

        @SerializedName("trip")
        @Expose
        private String trip;

        @SerializedName("username")
        @Expose
        private String username;

        @SerializedName("name")
        @Expose
        private String name;


        public Data(String registrationToken, String trip, String username, String name) {
            this.registrationToken = registrationToken;
            this.trip = trip;
            this.username = username;
            this.name = name;
        }

        public String getRegistrationToken() {
            return registrationToken;
        }

        public void setRegistrationToken(String registrationToken) {
            this.registrationToken = registrationToken;
        }

        public String getTrip() {
            return trip;
        }

        public void setTrip(String trip) {
            this.trip = trip;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
