package co.doodrideshare.dood.models;

import java.util.ArrayList;

/**
 * Created by Nishant on 11/5/2017.
 */

public class User {
    public String name;
    public String email;
    public String username;
    public String major;
    public String phoneNumber;
    public String registrationToken;
    public ArrayList<Trip> trips;

    public User() {
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public User(String name, String email, String major, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.major = major;
        this.phoneNumber = phoneNumber;
        this.username = email.substring(0, email.indexOf("@"));
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getMajor() {
        return major;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getRegistrationToken() {
        return registrationToken;
    }

    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    public String hashcode() {
        return name + "," + email + "," + username + "," + major + "," + phoneNumber + "," + registrationToken;
    }

    public User(String hashcode) {
        super();
        String[] items = hashcode.split(",");

        try {
            this.name = items[0];
            this.email = items[1];
            this.username = items[2];
            this.major = items[3];
            this.phoneNumber = items[4];
            this.registrationToken = items[5];
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Trip> getTrips() {
        return trips;
    }

    public void setTrips(ArrayList<Trip> trips) {
        this.trips = trips;
    }
}
