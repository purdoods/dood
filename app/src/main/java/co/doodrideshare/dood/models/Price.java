package co.doodrideshare.dood.models;

/**
 * Created by Nishant on 12/9/2017.
 */

public class Price {
    String indy2Chicago;
    String school2Chicago;
    String school2Indy;
    String school2Offcampus;
    String school2school;

    public Price() {

    }

    public String getIndy2Chicago() {
        return indy2Chicago;
    }

    public void setIndy2Chicago(String indy2Chicago) {
        this.indy2Chicago = indy2Chicago;
    }

    public String getSchool2Chicago() {
        return school2Chicago;
    }

    public void setSchool2Chicago(String school2Chicago) {
        this.school2Chicago = school2Chicago;
    }

    public String getSchool2Indy() {
        return school2Indy;
    }

    public void setSchool2Indy(String school2Indy) {
        this.school2Indy = school2Indy;
    }

    public String getSchool2Offcampus() {
        return school2Offcampus;
    }

    public void setSchool2Offcampus(String school2Offcampus) {
        this.school2Offcampus = school2Offcampus;
    }

    public String getSchool2school() {
        return school2school;
    }

    public void setSchool2school(String school2school) {
        this.school2school = school2school;
    }
}
