package co.doodrideshare.dood.models;

import android.support.annotation.NonNull;

/**
 * Created by Nishant on 11/5/2017.
 */

public class Trip implements Comparable<Trip> {
    private String from;
    private String to;
    private String driverUsername;
    private String riderUsername;
    private String time;
    private String id;
    private String type;

    public static String TYPE_OFFER = "offer";
    public static String TYPE_NEED = "need";

    public Trip() {
    }

    public Trip(String id, String from, String to, String username, String time, String type) {
        this.id = id;
        this.from = from;
        this.to = to;
        if (type.equals(Trip.TYPE_NEED)) {
            this.riderUsername = username;
            this.driverUsername = "";
        }
        else if (type.equals(Trip.TYPE_OFFER)) {
            this.driverUsername = username;
            this.riderUsername = "";
        }
        this.time = time;
        this.type = type;
    }

    public Trip(String hashcode) {
        super();
        String[] items = hashcode.split(",");

        try {
            this.from = items[0];
            this.to = items[1];
            this.riderUsername = items[2];
            this.driverUsername = items[3];
            this.time = items[4];
            this.id = items[5];
            this.type = items[6];
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int compareTo(@NonNull Trip that) {
        if (Double.parseDouble(this.getTime()) < Double.parseDouble(that.getTime()))
            return -1;
        else if (Double.parseDouble(this.getTime()) > Double.parseDouble(that.getTime()))
            return 1;
        else return 0;
    }

    public void setDriverUsername(String driverUsername) {
        this.driverUsername = driverUsername;
    }

    public String getDriverUsername() {
        return driverUsername;
    }

    public String getRiderUsername() {
        return riderUsername;
    }

    public String getTime() {
        return time;
    }

    public String hashcode() {
        return from + "," + to + "," + riderUsername + "," + driverUsername + "," + time + "," + id + "," + type;
    }

    public String getId() {
        return id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setRiderUsername(String riderUsername) {
        this.riderUsername = riderUsername;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
