package co.doodrideshare.dood.databaseHelper;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.UUID;

import co.doodrideshare.dood.activities.DestinationActivity;
import co.doodrideshare.dood.fragments.TripDialogFragment;
import co.doodrideshare.dood.models.Trip;
import co.doodrideshare.dood.models.User;
import co.doodrideshare.dood.utilities.Utility;
import okhttp3.internal.tls.TrustRootIndex;

/**
 * Created by Nishant on 11/5/2017.
 */

public class FirebaseDatabaseHelper {

    public static void updateUserInformation(DatabaseReference databaseReference, User user) {
        databaseReference.child("users").child(user.getUsername()).setValue(user);
    }

    public static void addTrip(Trip trip) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("trips");
        reference.child(trip.getId()).setValue(trip);
    }

    public static ArrayList<Trip> getTrips(DatabaseReference databaseReference, final Trip requestedTrip, Context context, final DestinationActivity.OnFetchCompleteListener fetchCompleteListener) {
        DatabaseReference reference = databaseReference.child("trips");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Trip> trips = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Trip currTrip = postSnapshot.getValue(Trip.class);
                    /* show only future trips or trips less than 30 min ago */
                    try {
                        // Maintaining compatibility with old version
                        if (currTrip.getType() == null) {
                            currTrip.setType(Trip.TYPE_OFFER);
                        }
                        if (Calendar.getInstance().getTimeInMillis() - Double.parseDouble(currTrip.getTime()) < 1800000)
                            trips.add(currTrip);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                trips = sortTrips(trips, requestedTrip);
                fetchCompleteListener.fetchComplete(trips);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return new ArrayList<>();
    }

    private static ArrayList<Trip> sortSubTrips(ArrayList<Trip> trips, Trip requestedTrip) {
        ArrayList<Trip> orderedTrips = new ArrayList<>();
        if (trips.isEmpty()) return orderedTrips;
        if (requestedTrip == null) {
            Collections.sort(trips);
            return trips;
        }

        ArrayList<Trip> tripsToSameLocation = new ArrayList<>();
        ArrayList<Trip> tripsToSameLocationFromSameLocation = new ArrayList<>();
        ArrayList<Trip> tripsFromSameLocation = new ArrayList<>();
        ArrayList<Trip> tripsWithNoMatch = new ArrayList<>();
        for (Trip trip : trips) {
            if (trip.getTo().equals(requestedTrip.getTo()) && trip.getFrom().equals(requestedTrip.getFrom())) {
                tripsToSameLocationFromSameLocation.add(trip);
            }
            else if (trip.getTo().equals(requestedTrip.getTo())) {
                tripsToSameLocation.add(trip);
            }
            else if (trip.getFrom().equals(requestedTrip.getFrom())) {
                tripsFromSameLocation.add(trip);
            }
            else {
                tripsWithNoMatch.add(trip);
            }
        }

        Collections.sort(tripsToSameLocationFromSameLocation);
        Collections.sort(tripsFromSameLocation);
        Collections.sort(tripsToSameLocation);
        Collections.sort(tripsWithNoMatch);
        orderedTrips.addAll(tripsToSameLocationFromSameLocation);
        orderedTrips.addAll(tripsToSameLocation);
        orderedTrips.addAll(tripsFromSameLocation);
        orderedTrips.addAll(tripsWithNoMatch);

        return orderedTrips;
    }

    private static ArrayList<Trip> sortTrips(ArrayList<Trip> trips, Trip requestedTrip) {
        ArrayList<Trip> orderedTrips = new ArrayList<>();
        boolean requestTripHasBeenDeleted = true;

        if (trips.isEmpty()) return orderedTrips;
        if (requestedTrip == null) {
            Collections.sort(trips);
            return trips;
        }

        ArrayList<Trip> needTrips = new ArrayList<>();
        ArrayList<Trip> offerTrips = new ArrayList<>();

        for (Trip trip: trips) {
            if (trip.getId().equals(requestedTrip.getId())) {
                requestTripHasBeenDeleted = false;
            }
            if (trip.getType().equals(Trip.TYPE_NEED)) {
                if (!trip.getId().equals(requestedTrip.getId())) {
                    needTrips.add(trip);
                }
            } else if (trip.getType().equals(Trip.TYPE_OFFER)) {
                if (!trip.getId().equals(requestedTrip.getId())) {
                    offerTrips.add(trip);
                }
            }
        }

        needTrips = sortSubTrips(needTrips, requestedTrip);
        offerTrips = sortSubTrips(offerTrips, requestedTrip);

        if (!requestTripHasBeenDeleted) {
            orderedTrips.add(requestedTrip);
        }

        if (requestedTrip.getType().equals(Trip.TYPE_NEED)) {
            orderedTrips.addAll(offerTrips);
            orderedTrips.addAll(needTrips);
        } else if (requestedTrip.getType().equals(Trip.TYPE_OFFER)) {
            orderedTrips.addAll(needTrips);
            orderedTrips.addAll(offerTrips);
        }

        return orderedTrips;
    }

    public static void getPrices(final DestinationActivity.OnPricesReceived listener) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference reference = databaseReference.child("prices");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.fetchComplete(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static ArrayList<Trip> getTrips(DatabaseReference databaseReference, final User user, Context context, final DestinationActivity.OnFetchCompleteListener fetchCompleteListener) {
        DatabaseReference reference = databaseReference.child("users").child(user.getUsername()).child("trips");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<Trip> trips = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    try {
                        Trip currTrip = postSnapshot.getValue(Trip.class);
                        /* show only future trips or trips less than 5 min ago */
                        trips.add(currTrip);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // SORT based on time of the trip
                trips = sortTrips(trips);
                fetchCompleteListener.fetchComplete(trips);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return new ArrayList<>();
    }

    private static ArrayList<Trip> sortTrips(ArrayList<Trip> trips) {
        // Sort trips based on destination and time
        return trips;
    }

    public static ArrayList<User> getUsers(DatabaseReference databaseReference, final DestinationActivity.OnUserFetchCompleteListener fetchCompleteListener) {
        DatabaseReference reference = databaseReference.child("users");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<User> users = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    users.add(postSnapshot.getValue(User.class));
                }
                fetchCompleteListener.fetchComplete(users);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return new ArrayList<>();
    }

    public static void updateRegTokenOnDatabase(DatabaseReference mDatabaseReference, String refreshedToken) {
        try {
            String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
            String username = email.substring(0, email.indexOf("@"));
            mDatabaseReference.child("users").child(username).child("registrationToken").setValue(refreshedToken);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static User getUserInfo(String username, final TripDialogFragment.OnUserFetchCompleteListener listener) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = databaseReference.child("users").child(username);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                listener.onUserFetchComplete(user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return null;
    }

    public static void addPotentialTripToDatabase(Trip mSelectedTrip, Context mContext) {
        if (mSelectedTrip.getType().equals(Trip.TYPE_OFFER)) {
            mSelectedTrip.setRiderUsername(Utility.getUser(mContext).getUsername());
        }
        else if (mSelectedTrip.getType().equals(Trip.TYPE_NEED)) {
            mSelectedTrip.setDriverUsername(Utility.getUser(mContext).getUsername());
        }
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("potentialTrips");
        databaseReference.child(mSelectedTrip.getId()).setValue(mSelectedTrip);
    }

    public static void sendFeedback(String username, String feedback) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("feedback").child(UUID.randomUUID().toString());
        databaseReference.child("username").setValue(username);
        databaseReference.child("feedback").setValue(feedback);
    }

    public static void deleteTrip(Trip trip) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("trips")
                .child(trip.getId());
        databaseReference.removeValue();
    }

    public static void addAcceptedTrips(Trip trip) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("acceptedTrips").child(trip.getId()).setValue(trip);
        databaseReference.child("trips").child(trip.getId()).removeValue();
    }
}
