package co.doodrideshare.dood.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import co.doodrideshare.dood.models.Price;
import co.doodrideshare.dood.models.User;

/**
 * Created by Nishant on 11/5/2017.
 */

public class Utility {

    private final static String REG_TOKEN = "registrationtoken";
    private final static String PREFERENCES = "preferences";
    private static final String USER = "USER";

    public static Price price = new Price();

    public static String getFormattedTime(Calendar calendar) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EE, MMM d, h:mm aa");
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getFormattedTime(Double time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time.longValue());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EE, MMM d\nh:mm aa");
        return simpleDateFormat.format(calendar.getTime());
    }

    public static void setUser(Context context, User user) {
        SharedPreferences prefs = context.getSharedPreferences(Utility.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Utility.USER, user.hashcode());
        editor.commit();
    }

    public static User getUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Utility.PREFERENCES, Context.MODE_PRIVATE);
        return new User(prefs.getString(Utility.USER, ""));
    }

    public static void setRegistrationToken(Context context, String refreshedToken) {
        SharedPreferences prefs = context.getSharedPreferences(Utility.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Utility.REG_TOKEN, refreshedToken);
        editor.commit();
    }

    public static String getRegistrationToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Utility.PREFERENCES, Context.MODE_PRIVATE);
        if (prefs.getString(Utility.REG_TOKEN, "").isEmpty()) {
            Utility.setRegistrationToken(context, FirebaseInstanceId.getInstance().getToken());
            return FirebaseInstanceId.getInstance().getToken();
        }
        else
            return prefs.getString(Utility.REG_TOKEN, "");
    }


    public static String getEstimatedPrice(String from, String to) {
        if (from.equals("EE") || from.equals("PMU") || from.equals("Corec")
                || from.equals("Wabash Landing") || from.equals("Chauncey")) {
            from = "school";
        }
        if (to.equals("EE") || to.equals("PMU") || to.equals("Corec")
                || to.equals("Wabash Landing") || to.equals("Chauncey")) {
            to = "school";
        }
        if (from.equals(to)) {
            return Utility.price.getSchool2school();
        }

        if (from.equals("Walmart") || from.equals("Avenue") || from.equals("Cottages") || from.equals("Univ. Crossing")) {
            from = "off-campus-housing";
        }
        if (to.equals("Walmart") || to.equals("Avenue") || to.equals("Cottages") || to.equals("Univ. Crossing")) {
            to = "off-campus-housing";
        }

        if (from.equals("off-campus-housing") && to.equals("school")) {
            return Utility.price.getSchool2Offcampus();
        }
        if (to.equals("off-campus-housing") && from.equals("school")) {
            return Utility.price.getSchool2Offcampus();
        }

        if (((from.equals("school") || from.equals("off-campus-housing")) && to.equals("Chicago"))
                || ((to.equals("school") || to.equals("off-campus-housing")) && from.equals("Chicago"))) {
            return Utility.price.getSchool2Chicago();
        }

        if (((from.equals("school") || from.equals("off-campus-housing")) && to.equals("Indy"))
                || ((to.equals("school") || to.equals("off-campus-housing")) && from.equals("Indy"))) {
            return Utility.price.getSchool2Indy();
        }

        if (from.equals("Chicago") && to.equals("Indy")) {
            return Utility.price.getIndy2Chicago();
        }
        if (to.equals("Chicago") && from.equals("Indy")) {
            return Utility.price.getIndy2Chicago();
        }

        return Utility.price.getSchool2school();
    }
}
